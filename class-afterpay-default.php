<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

/**
 * AfterPay Standard Payment Gateway
 *
 * Provides an common default for AfterPay Payment methods
 *
 * @class         WC_Gateway_Afterpay_Default
 * @extends        WC_Gateway_Afterpay
 * @package        WooCommerce/Classes/Payment
 * @author         Willem Fokkens
 */
class WC_Gateway_Afterpay_Default extends WC_Gateway_Afterpay {
	/**
	 * Constructor for the gateway.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {
		global $woocommerce;

		parent::__construct();

		$this->has_fields = true;

		// Load the form fields.
		$this->init_form_fields();

		// Load the settings.
		$this->init_settings();

		// Define user set variables
		$this->enabled         = ( isset( $this->settings['enabled'] ) ) ? $this->settings['enabled'] : '';
		$this->title           = ( isset( $this->settings['title'] ) ) ? $this->settings['title'] : '';
		$this->merchantid      = ( isset( $this->settings['merchantid'] ) ) ? $this->settings['merchantid'] : '';
		$this->portfolioid     = ( isset( $this->settings['portfolioid'] ) ) ? $this->settings['portfolioid'] : '';
		$this->password        = ( isset( $this->settings['password'] ) ) ? $this->settings['password'] : '';
		$this->lower_threshold = ( isset( $this->settings['lower_threshold'] ) ) ? $this->settings['lower_threshold'] : '';
		$this->upper_threshold = ( isset( $this->settings['upper_threshold'] ) ) ? $this->settings['upper_threshold'] : '';
		$this->testmode        = ( isset( $this->settings['testmode'] ) ) ? $this->settings['testmode'] : '';
		$this->debug_mail      = ( isset( $this->settings['debug_mail'] ) ) ? $this->settings['debug_mail'] : '';
		$this->show_phone      = ( isset( $this->settings['show_phone'] ) ) ? $this->settings['show_phone'] : '';
		$this->ip_restriction  = ( isset( $this->settings['ip_restriction'] ) ) ? $this->settings['ip_restriction'] : '';

		$afterpay_country       = 'NL';
		$afterpay_language      = 'NL';
		$afterpay_currency      = 'EUR';
		$afterpay_invoice_terms = 'https://www.afterpay.nl/algemene-voorwaarden';
		$afterpay_invoice_icon  = plugins_url( basename( dirname( __FILE__ ) ) . "/images/afterpay.png" );

		// Apply filters to Country and language
		$this->afterpay_country       = apply_filters( 'afterpay_country', $afterpay_country );
		$this->afterpay_language      = apply_filters( 'afterpay_language', $afterpay_language );
		$this->afterpay_currency      = apply_filters( 'afterpay_currency', $afterpay_currency );
		$this->afterpay_invoice_terms = apply_filters( 'afterpay_invoice_terms', $afterpay_invoice_terms );
		$this->icon                   = apply_filters( 'afterpay_invoice_icon', $afterpay_invoice_icon );
		$this->description            = __( 'AfterPay - Eenvoudig achteraf betalen', 'afterpay' );

		// Actions
		add_action( 'wp_enqueue_scripts', [ $this, 'enqueue_styles' ] );
		add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, [ $this, 'process_admin_options' ] );
		add_action( 'woocommerce_receipt_afterpay', [ &$this, 'receipt_page' ] );
	}

	/**
	 * Initialise Gateway Settings Form Fields
	 *
	 * @access public
	 * @return void
	 */
	public function init_form_fields() {
		$this->form_fields = apply_filters( 'afterpay_invoice_form_fields', [
			'enabled'         => [
				'title'   => __( 'Enable/Disable', 'afterpay' ),
				'type'    => 'checkbox',
				'label'   => __( 'Enable AfterPay NL Digital Invoice', 'afterpay' ),
				'default' => 'yes'
			],
			'title'           => [
				'title'       => __( 'Title', 'afterpay' ),
				'type'        => 'text',
				'description' => __( 'This controls the title which the user sees during checkout.', 'afterpay' ),
				'default'     => __( 'AfterPay - Veilig achteraf betalen', 'afterpay' )
			],
			'merchantid'      => [
				'title'       => __( 'Merchant ID', 'afterpay' ),
				'type'        => 'text',
				'description' => __( 'Please enter your AfterPay Merchant ID; this is needed in order to take payment!', 'afterpay' ),
				'default'     => ''
			],
			'portfolioid'     => [
				'title'       => __( 'Portfolio number', 'afterpay' ),
				'type'        => 'text',
				'description' => __( 'Please enter your AfterPay Portfolio Number; this is needed in order to take payment!', 'afterpay' ),
				'default'     => ''
			],
			'password'        => [
				'title'       => __( 'Portefeuille password', 'afterpay' ),
				'type'        => 'text',
				'description' => __( 'Please enter your AfterPay Portfolio Password; this is needed in order to take payment!', 'afterpay' ),
				'default'     => ''
			],
			'lower_threshold' => [
				'title'       => __( 'Lower threshold', 'afterpay' ),
				'type'        => 'text',
				'description' => __( 'Disable AfterPay Invoice if Cart Total is lower than the specified value. Leave blank to disable this feature.', 'afterpay' ),
				'default'     => ''
			],
			'upper_threshold' => [
				'title'       => __( 'Upper threshold', 'afterpay' ),
				'type'        => 'text',
				'description' => __( 'Disable AfterPay Invoice if Cart Total is higher than the specified value. Leave blank to disable this feature.', 'afterpay' ),
				'default'     => ''
			],
			'testmode'        => [
				'title'   => __( 'Test Mode', 'afterpay' ),
				'type'    => 'checkbox',
				'label'   => __( 'Enable AfterPay Test Mode. This will only work if you have a AfterPay test account. For test purchases with a live account.', 'afterpay' ),
				'default' => 'no'
			],
			'debug_mail'      => [
				'title'       => __( 'Debug mail', 'afterpay' ),
				'type'        => 'text',
				'description' => __( 'Use debug mail to send the complete AfterPay request to your mail, for debug functionality only. Leave empty to disable.', 'afterpay' ),
				'default'     => __( '' )
			],
			'show_phone'      => [
				'title'       => __( 'Show phone number', 'afterpay' ),
				'type'        => 'checkbox',
				'description' => __( 'Show phone number field instead of Woocommerce default field. If this field is missing in default checkout', 'afterpay' ),
				'default'     => 'no'
			],
			'ip_restriction'  => [
				'title'       => __( 'IP restriction', 'afterpay' ),
				'type'        => 'text',
				'description' => __( 'Fill in IP address to only show the payment method for that specific IP address. Leave empty to disable', 'afterpay' ),
				'default'     => __( '' )
			]
		] );
	}

	/**
	 * Show the admin options
	 *
	 * @access public
	 * @return void
	 */
	public function admin_options() {
		echo '
            <h3>' . __( 'AfterPay pay after delivery', 'afterpay' ) . '</h3>
            <p>' . __( 'With AfterPay your customers can pay after delivery. AfterPay works by adding extra personal information fields and then sending the details to AfterPay for verification.', 'afterpay' ) . '</p>
            <table class="form-table">
                ' . $this->generate_settings_html( [], false ) . '
            </table>
        ';
	}

	/**
	 * Check if this gateway is enabled and available in the user's country
	 *
	 * @access public
	 * @return boolean
	 */
	public function is_available() {
		global $woocommerce;

		if ( $this->enabled == "yes" ) {
			// Required fields check
			if ( ! $this->merchantid || ! $this->password ) {
				return false;
			}

			// Cart totals check - Lower threshold
			if ( $this->lower_threshold !== '' ) {
				if ( $woocommerce->cart->subtotal < $this->lower_threshold ) {
					return false;
				}
			}

			// Cart totals check - Upper threshold
			if ( $this->upper_threshold !== '' ) {
				if ( $woocommerce->cart->subtotal > $this->upper_threshold ) {
					return false;
				}
			}

			// Only activate the payment gateway if the customers country is the same as the filtered shop country ($this->afterpay_country)
			if ( $woocommerce->customer->get_country() != $this->afterpay_country ) {
				return false;
			}

			if ( $this->ip_restriction !== '' && $this->ip_restriction !== $this->get_client_ip() ) {
				return false;
			}

			return true;
		}

		return false;
	}

	/**
	 * Payment form on checkout page
	 *
	 * @acces public
	 * @return void
	 */
	public function payment_fields() {
		global $woocommerce;

		if ( $this->testmode == 'yes' ) {
			echo '<div class="afterpay-testmode">' . __( 'TEST MODE ENABLED', 'afterpay' ) . '</div>';
		}

		if ( $this->description ) {
			echo '<p>' . $this->description . '</p>';
		}

		echo '
            <fieldset>
                <p class="form-row">
                    <label for="' . $this->id . '_pno">' . __( 'Date of birth', 'afterpay' ) . ' <span class="required">*</span></label>
                    <span class="dob">
                        <select class="dob_select dob_day" name="' . $this->id . '_date_of_birth_day">
                            <option value="">' . __( 'Day', 'afterpay' ) . '</option>
        ';

		foreach ( range( 1, 31 ) as $day ) {
			$day_pad = str_pad( $day, 2, '0', STR_PAD_LEFT );
			echo '<option value="' . $day_pad . '">' . $day_pad . '</option>';
		}

		echo '
            </select>
            <select class="dob_select dob_month" name="' . $this->id . '_date_of_birth_month">
                <option value="">' . __( 'Month', 'afterpay' ) . '</option>
                <option value="01">' . __( 'Jan', 'afterpay' ) . '</option>
                <option value="02">' . __( 'Feb', 'afterpay' ) . '</option>
                <option value="03">' . __( 'Mar', 'afterpay' ) . '</option>
                <option value="04">' . __( 'Apr', 'afterpay' ) . '</option>
                <option value="05">' . __( 'May', 'afterpay' ) . '</option>
                <option value="06">' . __( 'Jun', 'afterpay' ) . '</option>
                <option value="07">' . __( 'Jul', 'afterpay' ) . '</option>
                <option value="08">' . __( 'Aug', 'afterpay' ) . '</option>
                <option value="09">' . __( 'Sep', 'afterpay' ) . '</option>
                <option value="10">' . __( 'Oct', 'afterpay' ) . '</option>
                <option value="11">' . __( 'Nov', 'afterpay' ) . '</option>
                <option value="12">' . __( 'Dec', 'afterpay' ) . '</option>
            </select>
            <select class="dob_select dob_year" name="' . $this->id . '_date_of_birth_year">
                <option value="">' . __( 'year', 'afterpay' ) . '</option>
        ';

		foreach ( range( 1920, ( date( 'Y' ) - 18 ) + 1 ) as $year ) {
			echo '<option value="' . $year . '">' . $year . '</option>';
		}

		echo '
                    </select>
                </span><!-- .dob -->
            </p>

            <div class="clear"></div>
        ';

		if ( $this->show_bankaccount ) {
			echo '
                <div class="clear"></div>

                <p class="form-row validate-required">
                    <label for="afterpay_bankaccount">' . __( 'Bankaccount', 'afterpay' ) . ' <span class="required">*</span></label>
                    <input type="text" class="input-text" name="' . $this->id . '_bankaccount" id="' . $this->id . '_bankaccount">
                </p>';
		}

		if ( $this->show_phone == 'yes' ) {
			echo '
                <div class="clear"></div>
    
                <p class="form-row form-row-first validate-required validate-phone">
                    <label for="' . $this->id . '_phone">' . __( 'Phone number', 'afterpay' ) . ' <span class="required">*</span></label>
                    <input type="text" class="input-text" name="' . $this->id . '_phone" id="' . $this->id . '_phone">
                </p>';
		}

		echo '
            <div class="clear"></div>

            <p class="form-row validate-required">
                <input type="checkbox" class="input-checkbox" name="' . $this->id . '_terms" id="' . $this->id . '_terms">
                
                <label for="' . $this->id . '_terms">
                    ' . __( 'I accept the', 'afterpay' ) . '
                    <a href="' . $this->afterpay_invoice_terms . '" target="blank">' . __( 'payment terms', 'afterpay' ) . '</a>
                    ' . __( ' from AfterPay.', 'afterpay' ) . ' <span class="required">*</span>
                </label>
            </p>

            <div class="clear"></div>

        </fieldset>';
	}

	/**
	 * Validate form fields.
	 *
	 * @access public
	 * @return boolean
	 */
	public function validate_fields() {
		global $woocommerce;

		if ( ! $_POST[ $this->id . '_date_of_birth_day' ] || ! $_POST[ $this->id . '_date_of_birth_month' ] || ! $_POST[ $this->id . '_date_of_birth_year' ] ) {
			wc_add_notice( __( 'Birthday is a required field', 'afterpay' ), 'error' );

			return false;
		}
		if ( ! $_POST[ $this->id . '_terms' ] ) {
			wc_add_notice( __( 'Please accept the AfterPay terms.', 'afterpay' ) );
		}

		if ( $this->show_bankaccount && ! $_POST[ $this->id . '_bankaccount' ] ) {
			wc_add_notice( __( 'Bankaccount is a required field', 'afterpay' ), 'error' );

			return false;
		}

		if ( $this->show_phone == "yes" && ! $_POST[ $this->id . '_phone' ] ) {
			wc_add_notice( __( 'Phone number is a required field', 'afterpay' ), 'error' );

			return false;
		}

		return true;
	}

	/**
	 * Process the payment and return the result
	 *
	 * @access public
	 *
	 * @param int $order_id
	 *
	 * @return array
	 **/
	public function process_payment( $order_id ) {
		global $woocommerce;

		$_tax = new WC_Tax();

		$order = new WC_order( $order_id );

		require_once( AFTERPAY_LIB . 'Afterpay.php' );

		// Create AfterPay object
		$afterpay = new \Afterpay\Afterpay();

		// Get values from afterpay form on checkout page

		// Set form fields per payment option

		// Collect the dob
		$afterpay_pno_day = isset( $_POST[ $this->id . '_date_of_birth_day' ] ) ? woocommerce_clean( $_POST[ $this->id . '_date_of_birth_day' ] ) : '';
		if ( $afterpay_pno_day == '' && $this->order_type != 'B2B' ) {
			wc_add_notice( __( 'Please enter a birthday' ), 'error' );
		}
		$afterpay_pno_month = isset( $_POST[ $this->id . '_date_of_birth_month' ] ) ? woocommerce_clean( $_POST[ $this->id . '_date_of_birth_month' ] ) : '';
		$afterpay_pno_year  = isset( $_POST[ $this->id . '_date_of_birth_year' ] ) ? woocommerce_clean( $_POST[ $this->id . '_date_of_birth_year' ] ) : '';

		if ( $this->order_type == 'B2B' ) {
			$afterpay_pno = '1970-01-01T00:00:00';
		} else {
			$afterpay_pno = $afterpay_pno_year . '-' . $afterpay_pno_month . '-' . $afterpay_pno_day . 'T00:00:00';
		}

		$afterpay_bankacount = isset( $_POST[ $this->id . '_bankaccount' ] ) ? woocommerce_clean( $_POST[ $this->id . '_bankaccount' ] ) : '';

		$afterpay_phone = isset( $_POST[ $this->id . '_phone' ] ) ? woocommerce_clean( $_POST[ $this->id . '_phone' ] ) : $order->billing_phone;

		if ( $this->order_type == 'B2B' ) {

			$afterpay_cocnumber   = isset( $_POST[ $this->id . '_cocnumber' ] ) ? woocommerce_clean( $_POST[ $this->id . '_cocnumber' ] ) : '';
			$afterpay_companyname = isset( $_POST[ $this->id . '_companyname' ] ) ? woocommerce_clean( $_POST[ $this->id . '_companyname' ] ) : '';
		}

		// Split address into House number and House extension for NL customers
		$afterpay_billing_address = $order->billing_address_1;
		$splitted_address         = $this->split_address( $afterpay_billing_address );

		$afterpay_billing_address         = $splitted_address[0];
		$afterpay_billing_house_number    = $splitted_address[1];
		$afterpay_billing_house_extension = $splitted_address[2];

		$afterpay_shipping_address = $order->shipping_address_1;
		$splitted_address          = $this->split_address( $afterpay_shipping_address );

		$afterpay_shipping_address         = $splitted_address[0];
		$afterpay_shipping_house_number    = $splitted_address[1];
		$afterpay_shipping_house_extension = $splitted_address[2];

		// Store afterpay specific form values in order as post meta
		update_post_meta( $order_id, 'afterpay_pno', $afterpay_pno );

		// Test mode or Live mode
		$afterpay_mode = $this->testmode == 'yes' ? 'test' : 'live';

		$authorisation['merchantid']  = $this->settings['merchantid'];
		$authorisation['portfolioid'] = $this->settings['portfolioid'];
		$authorisation['password']    = $this->settings['password'];

		// Create the order

		// Cart Contents
		if ( sizeof( $order->get_items() ) > 0 ) {
			foreach ( $order->get_items() as $item ) {
				if ( function_exists( 'get_product' ) ) {
					// Version 2.0
					$_product = $order->get_product_from_item( $item );

					// Get SKU or product id
					if ( ! $sku = $_product->get_sku() ) {
						$sku = $_product->id;
					}
				} else {
					// Version 1.6.6
					$_product = new WC_Product( $item['id'] );

					// Get SKU or product id
					if ( ! $sku = $_product->get_sku() ) {
						$sku = $item['id'];
					}
				}

				$item_tax_category = $this->get_tax_class( $order->get_line_total( $item, false ), $order->get_line_tax( $item ) );

				// apply_filters to item price so we can filter this if needed
				$afterpay_item_price_including_tax = $order->get_item_total( $item, true );
				$item_price                        = apply_filters( 'afterpay_item_price_including_tax', $afterpay_item_price_including_tax );
				$item_price                        = round( $item_price * 100, 0 );

				$afterpay->create_order_line( $sku, $item['name'], $item['qty'], $item_price, $item_tax_category );
			}
		}

		// Discount
		if ( $order->order_discount > 0 ) {
			// apply_filters to order discount so we can filter this if needed
			$afterpay_order_discount = $order->order_discount;
			$order_discount          = apply_filters( 'afterpay_order_discount', $afterpay_order_discount );
			$discount_sku            = __( 'Discount', 'afterpay' );
			$discount_description    = __( 'Discount on order', 'afterpay' );
			$discount_price          = round( $order_discount * - 100, 0 );

			$afterpay->create_order_line( $discount_sku, $discount_description, 1, $discount_price, 4 );
		}

		// Shipping
		if ( $order->order_shipping > 0 ) {
			// We manually calculate the shipping tax percentage here
			$calculated_shipping_tax_percentage = ( $order->order_shipping_tax / $order->order_shipping ) * 100;
			$calculated_shipping_tax_decimal    = ( $order->order_shipping_tax / $order->order_shipping ) + 1;
			$shipping_tax_rate                  = $this->get_tax_class( $order->get_total_shipping(), $order->get_shipping_tax() );

			// apply_filters to Shipping so we can filter this if needed
			$afterpay_shipping_price_including_tax = $order->order_shipping * $calculated_shipping_tax_decimal;
			$shipping_price                        = apply_filters( 'afterpay_shipping_price_including_tax', $afterpay_shipping_price_including_tax );
			$shipping_sku                          = __( 'Shipping', 'afterpay' );
			$shipping_description                  = __( 'Shipping on order', 'afterpay' );
			$shipping_price                        = round( $shipping_price * 100, 0 );

			$afterpay->create_order_line( $shipping_sku, $shipping_description, 1, $shipping_price, $shipping_tax_rate );
		}

		$fees = $woocommerce->cart->get_fees();

		if ( count( $fees ) > 0 ) {
			foreach ( $fees as $fee ) {
				$fee_sku         = __( 'Service Fee', 'afterpay' );
				$fee_description = $fee->name;
				$fee_price       = round( ( $fee->amount + $fee->tax ) * 100 );
				$afterpay->create_order_line( $fee_sku, $fee_description, 1, $fee_price, 1 );
			}
		}

		$aporder['billtoaddress']['city']                           = utf8_decode( $order->billing_city );
		$aporder['billtoaddress']['housenumber']                    = utf8_decode( $afterpay_billing_house_number );
		$aporder['billtoaddress']['housenumberaddition']            = utf8_decode( $afterpay_billing_house_extension );
		$aporder['billtoaddress']['isocountrycode']                 = $order->billing_country;
		$aporder['billtoaddress']['postalcode']                     = utf8_decode( $order->billing_postcode );
		$aporder['billtoaddress']['referenceperson']['dob']         = $afterpay_pno;
		$aporder['billtoaddress']['referenceperson']['email']       = $order->billing_email;
		$aporder['billtoaddress']['referenceperson']['gender']      = '';
		$aporder['billtoaddress']['referenceperson']['initials']    = utf8_decode( substr( $order->billing_first_name, 0, 1 ) );
		$aporder['billtoaddress']['referenceperson']['isolanguage'] = 'NL';
		$aporder['billtoaddress']['referenceperson']['lastname']    = utf8_decode( $order->billing_last_name );
		$aporder['billtoaddress']['referenceperson']['phonenumber'] = $afterpay_phone;
		$aporder['billtoaddress']['streetname']                     = utf8_decode( $afterpay_billing_address );

		// Shipping address
		if ( $order->get_shipping_method() == '' ) {
			// Use billing address if Shipping is disabled in Woocommerce
			$aporder['shiptoaddress'] = $aporder['billtoaddress'];
		} else {
			$aporder['shiptoaddress']['city']                           = utf8_decode( $order->shipping_city );
			$aporder['shiptoaddress']['housenumber']                    = utf8_decode( $afterpay_shipping_house_number );
			$aporder['shiptoaddress']['housenumberaddition']            = utf8_decode( $afterpay_shipping_house_extension );
			$aporder['shiptoaddress']['isocountrycode']                 = $order->shipping_country;
			$aporder['shiptoaddress']['postalcode']                     = utf8_decode( $order->shipping_postcode );
			$aporder['shiptoaddress']['referenceperson']['dob']         = $afterpay_pno;
			$aporder['shiptoaddress']['referenceperson']['email']       = $order->billing_email;
			$aporder['shiptoaddress']['referenceperson']['gender']      = '';
			$aporder['shiptoaddress']['referenceperson']['initials']    = utf8_decode( substr( $order->shipping_first_name, 0, 1 ) );
			$aporder['shiptoaddress']['referenceperson']['isolanguage'] = 'NL';
			$aporder['shiptoaddress']['referenceperson']['lastname']    = utf8_decode( $order->shipping_last_name );
			$aporder['shiptoaddress']['referenceperson']['phonenumber'] = $afterpay_phone;
			$aporder['shiptoaddress']['streetname']                     = utf8_decode( $afterpay_shipping_address );
		}

		$aporder['ordernumber']       = filter_var( $order->get_order_number(), FILTER_SANITIZE_NUMBER_INT );
		$aporder['bankaccountnumber'] = $afterpay_bankacount;
		$aporder['currency']          = 'EUR';
		$aporder['ipaddress']         = $this->get_client_ip();

		if ( $this->order_type == 'B2B' ) {
			$aporder['company']['cocnumber']   = $afterpay_cocnumber;
			$aporder['company']['companyname'] = $afterpay_companyname;

			$aporder['person']['dob']          = $afterpay_pno;
			$aporder['person']['emailaddress'] = $order->billing_email;
			$aporder['person']['initials']     = utf8_decode( substr( $order->billing_first_name, 0, 1 ) );
			$aporder['person']['isolanguage']  = 'NL';
			$aporder['person']['lastname']     = utf8_decode( $order->billing_last_name );
			$aporder['person']['phonenumber1'] = $afterpay_phone;

			$aporder['billtoaddress']['isolanguage'] = 'NL';
			$aporder['shiptoaddress']['isolanguage'] = 'NL';
		}

		try {
			// Transmit all the specified data, from the steps above, to afterpay.
			$afterpay->set_order( $aporder, $this->order_type );
			$afterpay->do_request( $authorisation, $afterpay_mode );

			$this->send_debug_mail( $afterpay );

			// Retreive response
			if ( isset( $afterpay->order_result->return->statusCode ) ) {
				switch ( $afterpay->order_result->return->statusCode ) {
					case 'A':
						$order->add_order_note( __( 'AfterPay payment completed.', 'afterpay' ) );

						// Payment complete
						$order->payment_complete();

						// Remove cart
						$woocommerce->cart->empty_cart();

						// Return thank you redirect
						return [
							'result'   => 'success',
							'redirect' => $this->get_return_url( $order )
						];

						break;
					case 'P':
						$order->add_order_note( __( 'AfterPay payment pending.', 'afterpay' ) );

						// Payment complete
						$order->update_status( 'on-hold', __( 'Awaiting AfterPay payment', 'afterpay' ) );

						// Remove cart
						$woocommerce->cart->empty_cart();

						// Return thank you redirect
						return [
							'result'   => 'success',
							'redirect' => $afterpay->order_result->return->extrafields->valueField
						];

						break;
					case 'W':
						//Order is denied, store it in a database.
						$order->add_order_note( __( 'AfterPay payment denied.', 'afterpay' ) );
						$order->add_order_note( __( $afterpay->order_result->return->messages[0]['message'], 'afterpay' ) );
						wc_add_notice( __( $afterpay->order_result->return->messages[0]['description'], 'afterpay' ), 'error' );

						// Cancel order to make new order possible
						$order->cancel_order();

						return;
						break;
				}
			} else {
				// Check for validation errors
				if ( $afterpay->order_result->return->resultId == '2' ) {
					//Unknown response, store it in a database
					$order->add_order_note( __( 'There is a problem with submitting this order to AfterPay.', 'afterpay' ) );
					$validationmsg = __( 'There is a problem with submitting this order to AfterPay, please check the following issues: ', 'afterpay' );
					$validationmsg .= '<ul>';
					if ( ! is_object( $afterpay->order_result->return->messages ) ) {
						foreach ( $afterpay->order_result->return->messages as $value ) {
							$validationmsg .= '<li style="list-style: inherit">' . __( $value['description'], 'afterpay' ) . '</li>';
							$order->add_order_note( __( $value['description'], 'afterpay' ) );
						}
					}
					$validationmsg .= '</ul>';

					wc_add_notice( $validationmsg, 'error' );
				} else {
					//Unknown response, store it in a database.
					$order->add_order_note( __( 'Unknown response from AfterPay.', 'afterpay' ) );
					wc_add_notice( __( 'Unknown response from AfterPay. Please contact our customer service', 'afterpay' ), 'error' );

					// Cancel order to make new order possible
					$order->cancel_order();
				}

				return;
			}
		} catch ( Exception $e ) {
			//The purchase was denied or something went wrong, print the message:
			wc_add_notice( sprintf( __( '%s (Error code: %s)', 'afterpay' ), utf8_encode( $e->getMessage() ), $e->getCode() ), 'error' );

			return;
		}
	}

	/**
	 * receipt_page
	 **/
	function receipt_page( $order ) {
		echo '<p>' . __( 'Thank you for your order, you will receive a payment invoice for your order from AfterPay.', 'afterpay' ) . '</p>';
	}

	function get_invoice_terms_link_text( $country ) {
		switch ( $country ) {
			case 'NL':
				$term_link_account = 'Factuurvoorwaarden';
				break;
			default:
				$term_link_account = __( 'Terms for Invoice', 'afterpay' );
		}

		return $term_link_account;
	} // end function get_account_terms_link_text()

	function send_debug_mail( $afterpay ) {
		if ( $this->debug_mail != '' ) {
			$afterpay->authorization->password = "REMOVED FOR SECURITY REASONS";
			wp_mail( $this->debug_mail, 'DEBUG MAIL WOOCOMMERCE AFTERPAY', print_r( $afterpay, 1 ) );
		}
	}

	function get_client_ip() {
		if ( array_key_exists( 'HTTP_X_FORWARDED_FOR', $_SERVER ) ) {
			$ipaddress = explode( ",", $_SERVER["HTTP_X_FORWARDED_FOR"] );

			return trim( $ipaddress[0] );
		} else {
			if ( array_key_exists( 'REMOTE_ADDR', $_SERVER ) ) {
				return $_SERVER["REMOTE_ADDR"];
			} else {
				if ( array_key_exists( 'HTTP_CLIENT_IP', $_SERVER ) ) {
					return $_SERVER["HTTP_CLIENT_IP"];
				}
			}
		}

		return '';
	}

	function get_tax_class( $total_amount, $tax_amount ) {
		// We manually calculate the tax percentage here
		if ( $tax_amount > 0 ) {
			// Calculate tax percentage
			$item_tax_percentage = number_format( ( $tax_amount / $total_amount ) * 100, 2, '.', '' );
		} else {
			$item_tax_percentage = 0.00;
		}

		if ( $item_tax_percentage > 7 ) {
			$item_tax_category = 1;
		} elseif ( $item_tax_percentage > 0 ) {
			$item_tax_category = 2;
		} else {
			$item_tax_category = 3;
		}

		return $item_tax_category;
	}

	function split_address( $address ) {
		// Get everything up to the first number with a regex
		$hasMatch = preg_match( '/^[^0-9]*/', $address, $match );

		// If no matching is possible, return the supplied string as the street
		if ( ! $hasMatch ) {
			return [ $address, "", "" ];
		}

		// Remove the street from the address.
		$address = str_replace( $match[0], "", $address );
		$street  = trim( $match[0] );

		// Nothing left to split, return
		if ( strlen( $address == 0 ) ) {
			return [ $street, "", "" ];
		}
		// Split the address to an array
		$addrArray = preg_split( "/([\s,--,\/,\\,|,,,+,_]+)|(?<=\d)(?=[a-z])|(?<=[a-z])(?=\d)/", $address );

		// Shift the first element off the array, that is the house number
		$housenumber = array_shift( $addrArray );

		// If the array is empty now, there is no extension.
		if ( count( $addrArray ) == 0 ) {
			return [ $street, $housenumber, "" ];
		}

		// Join together the remaining pieces as the extension.
		$extension = implode( " ", $addrArray );

		return [ $street, $housenumber, $extension ];
	}

	public function enqueue_styles() {
		wp_enqueue_style( 'woocommerce-gateway-afterpay', plugin_dir_url( __FILE__ ) . 'css/afterpay.css' );
	}
}