<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

/**
 * Belgian Open Invoice payment method for AfterPay
 *
 * @class         WC_Gateway_Afterpay_Belgium
 * @extends        WC_Gateway_Afterpay_Default
 * @package        WooCommerce/Classes/Payment
 * @author         Willem Fokkens
 */
class WC_Gateway_Afterpay_Belgium extends WC_Gateway_Afterpay_Default {
	/**
	 * Constructor for the gateway.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {
		global $woocommerce;

		parent::__construct();

		$this->id           = 'afterpay_beligum';
		$this->method_title = __( 'AfterPay BE Digital Invoice', 'afterpay' );
		$this->has_fields   = true;

		// Load the form fields.
		$this->init_form_fields();
		$this->show_bankaccount = false;
		$this->order_type       = 'B2C';

		// Load the settings.
		$this->init_settings();

		// Define user set variables
		$this->enabled         = ( isset( $this->settings['enabled'] ) ) ? $this->settings['enabled'] : '';
		$this->title           = ( isset( $this->settings['title'] ) ) ? $this->settings['title'] : '';
		$this->merchantid      = ( isset( $this->settings['merchantid'] ) ) ? $this->settings['merchantid'] : '';
		$this->portfolioid     = ( isset( $this->settings['portfolioid'] ) ) ? $this->settings['portfolioid'] : '';
		$this->password        = ( isset( $this->settings['password'] ) ) ? $this->settings['password'] : '';
		$this->lower_threshold = ( isset( $this->settings['lower_threshold'] ) ) ? $this->settings['lower_threshold'] : '';
		$this->upper_threshold = ( isset( $this->settings['upper_threshold'] ) ) ? $this->settings['upper_threshold'] : '';
		$this->testmode        = ( isset( $this->settings['testmode'] ) ) ? $this->settings['testmode'] : '';
		$this->debug_mail      = ( isset( $this->settings['debug_mail'] ) ) ? $this->settings['debug_mail'] : '';
		$this->show_phone      = ( isset( $this->settings['show_phone'] ) ) ? $this->settings['show_phone'] : '';
		$this->ip_restriction  = ( isset( $this->settings['ip_restriction'] ) ) ? $this->settings['ip_restriction'] : '';

		$afterpay_country       = 'BE';
		$afterpay_language      = 'NL';
		$afterpay_currency      = 'EUR';
		$afterpay_invoice_terms = 'https://www.afterpay.be/nl/klantenservice/betalingsvoorwaarden/';
		$afterpay_invoice_icon  = plugins_url( basename( dirname( __FILE__ ) ) . "/images/afterpay.png" );

		// Apply filters to Country and language
		$this->afterpay_country       = apply_filters( 'afterpay_country', $afterpay_country );
		$this->afterpay_language      = apply_filters( 'afterpay_language', $afterpay_language );
		$this->afterpay_currency      = apply_filters( 'afterpay_currency', $afterpay_currency );
		$this->afterpay_invoice_terms = apply_filters( 'afterpay_invoice_terms', $afterpay_invoice_terms );
		$this->icon                   = apply_filters( 'afterpay_invoice_icon', $afterpay_invoice_icon );
		$this->description            = 'U ontvangt per e-mail een betaalverzoek van AfterPay.';

		// Actions

		/* 2.0.0 */
		add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, [ $this, 'process_admin_options' ] );
		add_action( 'woocommerce_receipt_afterpay', [ &$this, 'receipt_page' ] );
		add_action( 'woocommerce_api_' . strtolower( get_class( $this ) ), [ &$this, 'push_state' ] );
	}

	/**
	 * Initialise Gateway Settings Form Fields
	 *
	 * @access public
	 * @return void
	 */
	function init_form_fields() {
		$this->form_fields = apply_filters( 'afterpay_belgium_form_fields', [
			'enabled'           => [
				'title'   => __( 'Enable/Disable', 'afterpay' ),
				'type'    => 'checkbox',
				'label'   => __( 'Enable AfterPay BE Digital Invoice', 'afterpay' ),
				'default' => 'no'
			],
			'title'             => [
				'title'       => __( 'Title', 'afterpay' ),
				'type'        => 'text',
				'description' => __( 'U ontvangt per e-mail een betaalverzoek van AfterPay.', 'afterpay' ),
				'default'     => __( 'AfterPay - Veilig achteraf betalen', 'afterpay' )
			],
			'merchantid'        => [
				'title'       => __( 'Merchant ID', 'afterpay' ),
				'type'        => 'text',
				'description' => __( 'Please enter your AfterPay Merchant ID; this is needed in order to take payment!', 'afterpay' ),
				'default'     => ''
			],
			'portfolioid'       => [
				'title'       => __( 'Portfolio number', 'afterpay' ),
				'type'        => 'text',
				'description' => __( 'Please enter your AfterPay Portfolio Number; this is needed in order to take payment!', 'afterpay' ),
				'default'     => ''
			],
			'password'          => [
				'title'       => __( 'Portefeuille password', 'afterpay' ),
				'type'        => 'text',
				'description' => __( 'Please enter your AfterPay Portfolio Password; this is needed in order to take payment!', 'afterpay' ),
				'default'     => ''
			],
			'lower_threshold'   => [
				'title'       => __( 'Lower threshold', 'afterpay' ),
				'type'        => 'text',
				'description' => __( 'Disable AfterPay Invoice if Cart Total is lower than the specified value. Leave blank to disable this feature.', 'afterpay' ),
				'default'     => '5'
			],
			'upper_threshold'   => [
				'title'       => __( 'Upper threshold', 'afterpay' ),
				'type'        => 'text',
				'description' => __( 'Disable AfterPay Invoice if Cart Total is higher than the specified value. Leave blank to disable this feature.', 'afterpay' ),
				'default'     => ''
			],
			'testmode'          => [
				'title'   => __( 'Test Mode', 'afterpay' ),
				'type'    => 'checkbox',
				'label'   => __( 'Enable AfterPay Test Mode. This will only work if you have a AfterPay test account. For test purchases with a live account.', 'afterpay' ),
				'default' => 'yes'
			],
			'debug_mail'        => [
				'title'       => __( 'Debug mail', 'afterpay' ),
				'type'        => 'text',
				'description' => __( 'Use debug mail to send the complete AfterPay request to your mail, for debug functionality only. Leave empty to disable.', 'afterpay' ),
				'default'     => __( '' )
			],
			'order_update_mail' => [
				'title'       => __( 'Order update mail', 'afterpay' ),
				'type'        => 'text',
				'description' => __( 'Use order update mail to send a notification mail when the order is accepted after pending state. Leave empty to disable.', 'afterpay' ),
				'default'     => __( '' )
			],
			'show_phone'        => [
				'title'       => __( 'Show phone number', 'afterpay' ),
				'type'        => 'checkbox',
				'description' => __( 'Show phone number field instead of Woocommerce default field. If this field is missing in default checkout', 'afterpay' ),
				'default'     => 'no'
			],
			'ip_restriction'    => [
				'title'       => __( 'IP restriction', 'afterpay' ),
				'type'        => 'text',
				'description' => __( 'Fill in IP address to only show the payment method for that specific IP address. Leave empty to disable', 'afterpay' ),
				'default'     => __( '' )
			]
		] );
	}

	function push_state() {
		global $woocommerce;

		// Check if POST contains all values, else quit
		if ( ! isset( $_POST['merchantId'] ) || ! isset( $_POST['portefeuilleId'] ) || ! isset( $_POST['orderReference'] ) || ! isset( $_POST['statusCode'] ) || ! isset( $_POST['signature'] ) ) {
			$pushlog = 'Required field is missing';
			$this->mail_log( $pushlog );

			return;
		}

		$hash = $_POST['merchantId'] . $_POST['portefeuilleId'] . $this->password . $_POST['orderReference'] . $_POST['statusCode'];
		$pushlog .= "\n Hash: " . $hash;

		$checksum = md5( $hash );
		$pushlog .= "\n Checksum: " . $checksum;

		$signature = $_POST['signature'];
		$pushlog .= "\n Signature: " . $signature;

		// Check if Checksum is the same as the signature
		if ( $signature !== $checksum ) {
			$pushlog .= "\n" . 'Checksum does not match signature';
			$this->mail_log( $pushlog );

			return;
		}

		$pushlog .= "\n Signature: " . $_POST['orderReference'];

		$order = new WC_order( $_POST['orderReference'] );

		$pushlog .= print_r( $order, 1 );

		if ( $order->get_status() != 'on-hold' ) {
			$pushlog .= "\n" . 'Order ' . $_POST['orderReference'] . 'does not exists or does not have the state: on-hold';
			$this->mail_log( $pushlog );

			return;
		}

		switch ( $_POST['statusCode'] ) {
			case 'A':
				$pushlog .= "\n" . 'The status is A, so try to complete the payment of the order';
				// Set Payment complete
				$order->add_order_note( __( 'The order is accepted by AfterPay and can be processed', 'afterpay' ) );
				$order->payment_complete();

				// Sent notification to admin
				wp_mail( $this->order_update_mail, 'ORDER ' . $_POST['orderReference'] . ' is accepted by AfterPay and can be processed', 'Dear sir/madam,
                
                The order ' . $_POST['orderReference'] . ' is accepted by AfterPay and can be processed.
                
                With regards,
                
                AfterPay / Woocommerce Plugin' );

				$this->mail_log( $pushlog );
				break;
			case 'W':
				$pushlog .= "\n" . 'The status is W, so try to cancel the order';
				// Cancel the order
				$order->add_order_note( __( 'The order is rejected by AfterPay and can be cancelled', 'afterpay' ) );
				$order->cancel_order();

				$this->mail_log( $pushlog );
				break;
			case 'V':
				$pushlog .= "\n" . 'The status is V, so try to cancel the order';
				// Cancel the order
				$order->add_order_note( __( 'The order is removed by AfterPay and can be cancelled', 'afterpay' ) );
				$order->cancel_order();

				$this->mail_log( $pushlog );
				break;
			case 'P':
				$pushlog .= "\n" . 'The status is still pending, so do nothing';
				$order->add_order_note( __( 'The order is still pending by AfterPay', 'afterpay' ) );
				$this->mail_log( $pushlog );
				break;
			default:
				$pushlog .= "\n" . 'Error in push, statuscode is missing';
				$this->mail_log( $pushlog );
				break;
		}
	}

	function mail_log( $pushlog ) {
		global $woocommerce;
		if ( $this->debug_mail != '' ) {
			wp_mail( $this->debug_mail, 'WOOCOMMERCE PUSH LOG', $pushlog );
		}
	}
}