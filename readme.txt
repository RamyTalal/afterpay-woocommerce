=== Plugin Name ===
Contributors: afterpay
Tags: afterpay, payment, woocommerce
Requires at least: 4.5.0
Tested up to: 4.7
Stable tag: 4.7
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

AfterPay is the most consumer-friendly post-payment method in the Netherlands and Belgium. With this plugin you can provide this payment method.

== Description ==

AfterPay provides the most consumer-friendly post-payment method in the Netherlands and Belgium. With this extension you can provide this pay-after-delivery payment method to your customers. Providing this solution means that customers can order the products they want, and pay after they have received it, without the non-payment risk for the merchant.

A separate AfterPay account is required to use this extension and additional fees apply. The pricing of the AfterPay service is flexible and based on risk and sales factors. Pricing starts at € 1.35 fixed fee and 3.4% variabel fee per total order amount.

The most used reason for consumers to use AfterPay is the ease of using the payment method.  For example, a customer orders two pair of shoes, returns one pair, then the customer only has to pay for the pair they are keeping. Because AfterPay handles a 14 day payment term, there is enough time to settle the final payment with AfterPay.  

The extension provides the new payment methods in your Magento environment, which will process the payment to the AfterPay service and handles the response as a customer friendly feedback. With this extension you can enable AfterPay for consumers and businesses in the Netherlands and AfterPay for consumers in Belgium.

== Installation ==

This section describes how to install the plugin and get it working.

1. Upload the plugin files to the `/wp-content/plugins/plugin-name` directory, or install the plugin through the WordPress plugins screen directly.
2. Activate the plugin through the 'Plugins' screen in WordPress
3. Use the Woocommerce->Settings->Checkout screen to configure the AfterPay Payment Methods

== Changelog ==

2017.01.05 - version 2.8
 * Removed css from checkout and changed field order
 * When a validation error occurs, than the order will not be cancelled
 * Added new copyright notice
 * Updated AfterPay core library to 1.1.8
 * compatible with WP 4.7 + WooCommerce 2.6.11

2016.09.05 - version 2.7
 * fix for validations in Belgium
 * used updated version of composer library

2016.08-26 - version 2.6
 * fix for default country, removed option to base housenumber on default country
 * fix for calculation of tax rate for shipping based on tax amount

2016.08.09 - version 2.5
 * fix for housenumberaddition, now works with space (10 a), nospace (10a), special chars (- + , | )
 * fix for vat category when rounding errors
 * translation for validation problem with bankaccount numbers

2016.06.13 - version 2.4
 * fix for issue where housenumber and housenumberaddition are not send separately
 * fix for issue where B2B orders fail due to missing gender field
 * removed unneeded B2B fields
 * fix for order status not updating on Belgium orders, using WP 4.5.3
 * added Dutch translations for all error messages
 * compatible with WP 4.5.3 + WooCommerce 2.6.2

2016.04.05 - version 2.3
 * Removed automatic update functionality to comply Woocommerce Standards
 * Tested compatibility with Woocommerce 2.5.5
 * Core AfterPay Class = Composer / Packagist class 1.0.8: https://packagist.org/packages/payintegrator/afterpay
 * Changed naming and description of payment methods
 * Removed changable option for description
 * Removed OsPinto dBug Class
 * Added most recent AfterPay Logo

2016.02-09 - version 2.2
 * Added AfterPay NL Business 2 Business
 * Tested compatibility with Woocommerce 2.4.2
 * Several small bugfixes

2015.06.11 - version 2.1
 * Added automatic update functionality

2015.06.04 - version 2.0.2
 * Added AfterPay Belgium
 * Update validation errors
 * Added posibility for showing phone number
 * Added posibility for ip restriction in testing
 * Added better way for requesting client IP address
 * Tested compatibility with Woocommerce 2.3.0
 * Several small bugfixes

2015.04.22 - version 1.9.1
 * Fixed add_error problems
 * Fixed several php warning problems

2015.04.13 - version 1.9
 * Fixed add_error problems

2015.01.19 - version 1.8
 * Added compatibility with http://www.woothemes.com/products/sequential-order-numbers-pro/

2014.10.07 - version 1.7
 * Removed surcharge code, advice to use https://wordpress.org/plugins/woocommerce-add-extra-charges-option-to-payment-gateways/
 * Fixed WP DEBUG messages

2014.05.06 - version 1.6
 * Code cleaned and set to WooCommerce standards. Also added default gateway to extend other payment methods. And payment fee added to cart.

2014.04.30 - version 1.5
 * Added AfterPay Direct Debit
 * Fixed invoice fee, added as new surcharge method. Now visible in checkout.

2014.04.24 - version 1.4
 * Fixed issue with invoice fee, only added with AfterPay orders
 * Correct response for validation errors

2014.04.10 - version 1.3
 * Added phone number format to AfterPay Library

2014.04.10 - version 1.2
 * Fixed return url problem

2014.02.11 - version 1.1
 * Removed bankaccount number for SEPA
 
2013.06.19 - version 1.0
 * First Release