<?php

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

/**
 * Direct Debit payment method for AfterPay
 *
 * @class         WC_Gateway_Afterpay_Directdebit
 * @extends        WC_Gateway_Afterpay_Default
 * @package        WooCommerce/Classes/Payment
 * @author         Willem Fokkens
 */
class WC_Gateway_Afterpay_Directdebit extends WC_Gateway_Afterpay_Default {
	/**
	 * Constructor for the gateway.
	 *
	 * @access public
	 * @return void
	 */
	public function __construct() {
		global $woocommerce;

		parent::__construct();

		$this->id           = 'afterpay_directdebit';
		$this->method_title = __( 'AfterPay NL SEPA Direct Debit', 'afterpay' );

		// Load the form fields.
		$this->init_form_fields();
		$this->show_bankaccount = true;
		$this->order_type       = 'B2C';

		// Load the settings.
		$this->init_settings();

		// Define user set variables
		$this->enabled         = ( isset( $this->settings['enabled'] ) ) ? $this->settings['enabled'] : '';
		$this->title           = ( isset( $this->settings['title'] ) ) ? $this->settings['title'] : '';
		$this->merchantid      = ( isset( $this->settings['merchantid'] ) ) ? $this->settings['merchantid'] : '';
		$this->portfolioid     = ( isset( $this->settings['portfolioid'] ) ) ? $this->settings['portfolioid'] : '';
		$this->password        = ( isset( $this->settings['password'] ) ) ? $this->settings['password'] : '';
		$this->lower_threshold = ( isset( $this->settings['lower_threshold'] ) ) ? $this->settings['lower_threshold'] : '';
		$this->upper_threshold = ( isset( $this->settings['upper_threshold'] ) ) ? $this->settings['upper_threshold'] : '';
		$this->testmode        = ( isset( $this->settings['testmode'] ) ) ? $this->settings['testmode'] : '';
		$this->debug_mail      = ( isset( $this->settings['debug_mail'] ) ) ? $this->settings['debug_mail'] : '';
		$this->show_phone      = ( isset( $this->settings['show_phone'] ) ) ? $this->settings['show_phone'] : '';
		$this->ip_restriction  = ( isset( $this->settings['ip_restriction'] ) ) ? $this->settings['ip_restriction'] : '';

		$afterpay_country       = 'NL';
		$afterpay_language      = 'NL';
		$afterpay_currency      = 'EUR';
		$afterpay_invoice_terms = 'https://www.afterpay.nl/algemene-voorwaarden';
		$afterpay_invoice_icon  = plugins_url( basename( dirname( __FILE__ ) ) . "/images/afterpay.png" );

		// Apply filters to Country and language
		$this->afterpay_country       = apply_filters( 'afterpay_country', $afterpay_country );
		$this->afterpay_language      = apply_filters( 'afterpay_language', $afterpay_language );
		$this->afterpay_currency      = apply_filters( 'afterpay_currency', $afterpay_currency );
		$this->afterpay_invoice_terms = apply_filters( 'afterpay_invoice_terms', $afterpay_invoice_terms );
		$this->icon                   = apply_filters( 'afterpay_invoice_icon', $afterpay_invoice_icon );
		$this->description            = 'U kunt met AfterPay betalen tot een bedrag van € ' . $this->upper_threshold . '. Het bedrag wordt automatisch afgeschreven van het opgegeven rekeningnummer.';

		// Actions

		/* 2.0.0 */
		add_action( 'woocommerce_update_options_payment_gateways_' . $this->id, [ $this, 'process_admin_options' ] );
		add_action( 'woocommerce_receipt_afterpay', [ &$this, 'receipt_page' ] );
	}

	/**
	 * Initialise Gateway Settings Form Fields
	 *
	 * @access public
	 * @return void
	 */
	public function init_form_fields() {
		$this->form_fields = apply_filters( 'afterpay_directdebit_form_fields', [
			'enabled'         => [
				'title'   => __( 'Enable/Disable', 'afterpay' ),
				'type'    => 'checkbox',
				'label'   => __( 'Enable AfterPay NL SEPA Direct Debit', 'afterpay' ),
				'default' => 'no'
			],
			'title'           => [
				'title'       => __( 'Title', 'afterpay' ),
				'type'        => 'text',
				'description' => __( 'This controls the title which the user sees during checkout.', 'afterpay' ),
				'default'     => __( 'AfterPay - Veilig achteraf betalen (eenmalige machtiging)', 'afterpay' )
			],
			'merchantid'      => [
				'title'       => __( 'Merchant ID', 'afterpay' ),
				'type'        => 'text',
				'description' => __( 'Please enter your AfterPay Merchant ID; this is needed in order to take payment!', 'afterpay' ),
				'default'     => ''
			],
			'portfolioid'     => [
				'title'       => __( 'Portfolio number', 'afterpay' ),
				'type'        => 'text',
				'description' => __( 'Please enter your AfterPay Portfolio Number; this is needed in order to take payment!', 'afterpay' ),
				'default'     => ''
			],
			'password'        => [
				'title'       => __( 'Portefeuille password', 'afterpay' ),
				'type'        => 'text',
				'description' => __( 'Please enter your AfterPay Portfolio Password; this is needed in order to take payment!', 'afterpay' ),
				'default'     => ''
			],
			'lower_threshold' => [
				'title'       => __( 'Lower threshold', 'afterpay' ),
				'type'        => 'text',
				'description' => __( 'Disable AfterPay Invoice if Cart Total is lower than the specified value. Leave blank to disable this feature.', 'afterpay' ),
				'default'     => '5'
			],
			'upper_threshold' => [
				'title'       => __( 'Upper threshold', 'afterpay' ),
				'type'        => 'text',
				'description' => __( 'Disable AfterPay Invoice if Cart Total is higher than the specified value. Leave blank to disable this feature.', 'afterpay' ),
				'default'     => ''
			],
			'testmode'        => [
				'title'   => __( 'Test Mode', 'afterpay' ),
				'type'    => 'checkbox',
				'label'   => __( 'Enable AfterPay Test Mode. This will only work if you have a AfterPay test account. For test purchases with a live account.', 'afterpay' ),
				'default' => 'yes'
			],
			'debug_mail'      => [
				'title'       => __( 'Debug mail', 'afterpay' ),
				'type'        => 'text',
				'description' => __( 'Use debug mail to send the complete AfterPay request to your mail, for debug functionality only. Leave empty to disable.', 'afterpay' ),
				'default'     => __( '' )
			],
			'show_phone'      => [
				'title'       => __( 'Show phone number', 'afterpay' ),
				'type'        => 'checkbox',
				'description' => __( 'Show phone number field instead of Woocommerce default field. If this field is missing in default checkout', 'afterpay' ),
				'default'     => 'no'
			],
			'ip_restriction'  => [
				'title'       => __( 'IP restriction', 'afterpay' ),
				'type'        => 'text',
				'description' => __( 'Fill in IP address to only show the payment method for that specific IP address. Leave empty to disable', 'afterpay' ),
				'default'     => __( '' )
			]
		] );
	}
}