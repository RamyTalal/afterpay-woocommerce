<?php
/*
Plugin Name: WooCommerce AfterPay Gateway
Plugin URI: https://www.afterpay.nl
Description: Extends WooCommerce. Provides a <a href="http://www.afterpay.nl" target="_blank">AfterPay</a> gateway for WooCommerce.
Version: 2.8
Author: Willem Fokkens
Author URI: https://www.linkedin.com/in/willemfokkens
*/

/**
 * Copyright (c) 2011-2017  arvato Finance B.V.
 *
 * AfterPay reserves all rights in the Program as delivered. The Program
 * or any portion thereof may not be reproduced in any form whatsoever without
 * the written consent of AfterPay.
 *
 * Disclaimer:
 * THIS NOTICE MAY NOT BE REMOVED FROM THE PROGRAM BY ANY USER THEREOF.
 * THE PROGRAM IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS
 * OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL
 * THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE PROGRAM OR THE USE OR OTHER DEALINGS
 * IN THE PROGRAM.
 *
 * @copyright   Copyright (c) 2011-2017 arvato Finance B.V.
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
} // Exit if accessed directly

/**
 * Required functions
 */
if ( ! function_exists( 'woothemes_queue_update' ) ) {
	require_once( 'woo-includes/woo-functions.php' );
}

/**
 * Plugin updates
 */
//woothemes_queue_update( plugin_basename( __FILE__ ), '', '' );

function init_afterpay_gateway() {
	if ( ! class_exists( 'WC_Payment_Gateway' ) ) {
		return;
	}

	/**
	 * Localisation
	 */
	load_plugin_textdomain( 'afterpay', false, dirname( plugin_basename( __FILE__ ) ) . '/languages/' );

	// Define AfterPay root Dir
	define( 'AFTERPAY_DIR', dirname( __FILE__ ) . '/' );

	// Define AfterPay lib Dir
	define( 'AFTERPAY_LIB', dirname( __FILE__ ) . '/vendor/payintegrator/afterpay/lib/' );

	/**
	 * AfterPay Payment Gateway
	 *
	 * @class         WC_Gateway_Afterpay
	 * @extends        WC_Payment_Gateway
	 * @package        WooCommerce/Classes/Payment
	 * @author         Willem Fokkens
	 */
	class WC_Gateway_Afterpay extends WC_Payment_Gateway {
		/**
		 * Constructor for the gateway.
		 *
		 * @access public
		 * @return void
		 */
		public function __construct() {
			global $woocommerce;
		}
	}

	// Include AfterPay specific payment classes
	require_once 'class-afterpay-default.php';
	require_once 'class-afterpay-openinvoice.php';
	require_once 'class-afterpay-directdebit.php';
	require_once 'class-afterpay-business.php';
	require_once 'class-afterpay-belgium.php';
}

// Actions
add_action( 'plugins_loaded', 'init_afterpay_gateway', 0 );

/**
 * Add the gateway to WooCommerce
 *
 * @access public
 * @return array
 **/
function add_afterpay_gateway( $methods ) {
	$methods[] = 'WC_Gateway_Afterpay_Openinvoice';
	$methods[] = 'WC_Gateway_Afterpay_Directdebit';
	$methods[] = 'WC_Gateway_Afterpay_Business';
	$methods[] = 'WC_Gateway_Afterpay_Belgium';

	return $methods;
}

// Filters
add_filter( 'woocommerce_payment_gateways', 'add_afterpay_gateway' );