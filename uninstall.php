<?php
/**
 * WooCommerce AfterPay Gateway
 * By Willem Fokkens (me@willemfokkens.com)
 *
 * Uninstall - removes all AfterPay options from DB when user deletes the plugin via WordPress backend.
 * @since 0.3
 **/

if ( ! defined( 'WP_UNINSTALL_PLUGIN' ) ) {
	exit();
}

delete_option( 'woocommerce_afterpay_settings' );